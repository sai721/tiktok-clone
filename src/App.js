import React, { useState, useEffect } from "react";
import videoFile from "./videos/ledani.mp4";
import videoFile2 from "./videos/pubg.mp4";

import "./App.css";
import Video from "./Video";
import axios from "./axios";
import { CircularProgress } from "@material-ui/core";

function App() {
  const [videos, setVideos] = useState([]);
  useEffect(() => {
    async function fetchPosts() {
      const response = await axios.get("/v2/posts");
      setVideos(response.data);
      console.log(response.data);
      return response;
    }
    fetchPosts();
  }, []);

  return (
    <div className="app">
      <div className="app__videos">
        {!videos.length ? (
          <div className="app__progress">
            <CircularProgress />
          </div>
        ) : (
          videos.map(
            (
              { url, channel, description, song, likes, messages, shares },
              index
            ) => (
              <Video
                key={index}
                url={url}
                channel={channel}
                description={description}
                song={song}
                likes={likes}
                messages={messages}
                shares={shares}
              />
            )
          )
        )}
      </div>
    </div>
  );
}

export default App;
