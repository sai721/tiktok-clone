import React, { useRef, useState, useEffect } from "react";
import "./video.css";
import VideoFooter from "./VideoFooter";
import VideoSidebar from "./VideoSidebar";

function Video({ url, channel, description, song, likes, messages, shares }) {
  const [playing, setPlaying] = useState(false);
  const videoRef = useRef(null);

  useEffect(() => {
    return () => {
      videoRef.current.pause();
      setPlaying(false);
    };
  }, []);
  const handleVideoPress = () => {
    if (playing) {
      console.log(videoRef);

      videoRef.current.pause();
      setPlaying(false);
    } else {
      videoRef.current.play();
      setPlaying(true);
    }
  };
  const handleScroll = () => {
    console.log("scrolled");
    if (playing) {
      console.log(videoRef);

      videoRef.current.pause();
      setPlaying(false);
    }
  };
  return (
    <div className="video" onTouchMove={handleScroll}>
      <video
        src={url}
        loop
        ref={videoRef}
        onClick={handleVideoPress}
        className="video__player"
      ></video>

      <VideoFooter channel={channel} description={description} song={song} />

      <VideoSidebar likes={likes} messages={messages} shares={shares} />
    </div>
  );
}

export default Video;
